// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Aula06.h"
#include "EstruturaDadoMapa.generated.h"

USTRUCT(BlueprintType) struct FEstruturaDadoMapa : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "Aula06Interface|Aula06 Estruturas")
	FString WAM_BlockDescription;

	UPROPERTY(BlueprintReadOnly, Category = "Aula06Interface|Aula06 Estruturas")
	int32   WAM_BlockPosX;

	UPROPERTY(BlueprintReadOnly, Category = "Aula06Interface|Aula06 Estruturas")
	int32   WAM_BlockPosY;

	UPROPERTY(BlueprintReadOnly, Category = "Aula06Interface|Aula06 Estruturas")
	int32   WAM_BlockType;
};