// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "EstruturaDadoMapa.h"
#include "Aula07InterfaceJogo.generated.h"

USTRUCT(BlueprintType, Category = "Aula06Interface|Aula06 Estruturas")
struct FAula06EstruturaAtor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aula06Interface|Aula06 Estruturas")
	FVector        vPosicaoAtor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aula06Interface|Aula06 Estruturas")
	int            iTipoAtor;
};

USTRUCT(BlueprintType, Category = "Aula06Interface|Aula06 Estruturas")
struct FAula06EstruturaJogo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aula06Interface|Aula06 Estruturas")
	TArray<FEstruturaDadoMapa> vAula06_Blocos;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aula06Interface|Aula06 Estruturas")
	FAula06EstruturaAtor    sAula06Player01;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aula06Interface|Aula06 Estruturas")
	FAula06EstruturaAtor    sAula06Player02;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aula06Interface|Aula06 Estruturas")
	UDataTable*             tAula06PlanilhaLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aula06Interface|Aula06 Estruturas")
	FVector                  tAula06CentroMapa;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aula06Interface|Aula06 Estruturas")
	int                      tAula06TamanhoMapa;
};

/**
 * 
 */
UCLASS()
class AULA06_API UAula07InterfaceJogo : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/********************************************************/
	/*Funcao de inicializacao do jogo                       */
	/********************************************************/
	UFUNCTION(BlueprintCallable, Category = "Aula06Interface|Aula06 Funcoes")
	static bool Aula06InicializaJogo(FAula06EstruturaJogo pDadosJogo)
	{
		return true;
	}

	/********************************************************/
	/*Funcao de inicializacao do jogo                       */
	/********************************************************/
	UFUNCTION(BlueprintCallable, Category = "Aula06Interface|Aula06 Funcoes")
	static TArray<FEstruturaDadoMapa> Aula06PegaParedes()
	{
		TArray <FEstruturaDadoMapa> tParedes;
		FEstruturaDadoMapa tBloco;

		for (int i = 0;i < 10;i++)
		{
			for (int j = 0;j < 10;j++)
			{
				tBloco.WAM_BlockDescription = "Bloco";
				tBloco.WAM_BlockPosX = i;
				tBloco.WAM_BlockPosY = j;
				tBloco.WAM_BlockType = 0;

				tParedes.Push(tBloco);
			}
		}
		return tParedes;
	}

	/********************************************************/
	/*Funcao de inicializacao do jogo                       */
	/********************************************************/
	UFUNCTION(BlueprintCallable, Category = "Aula06Interface|Aula06 Funcoes")
	static bool Aula06IncluiParede(FEstruturaDadoMapa pBloco)
	{
		pParedes.Push(pBloco);
		return true;
	}



	static TArray<FEstruturaDadoMapa>   pParedes;
};
